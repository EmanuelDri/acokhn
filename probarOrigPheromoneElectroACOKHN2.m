estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[400]
    for numeroDeHormigas=[400]
        for rho=[.1]
            %             for alpha=1:5
            %                 for beta=1:5
            for alpha=1
                for Q=4
                    for ejecucion=1:2;
                        
                        tic
                        
                        [path,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACO(8,rho,numeroDeIteraciones,numeroDeHormigas,3,2000*pi,1/sqrt(2),.1,3,alpha,0,5,'uniform',2,3);
                        linea=num2str([path,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeIteraciones numeroDeHormigas ejecucion]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_new_renegade_improving_5'  '.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc
                        
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
