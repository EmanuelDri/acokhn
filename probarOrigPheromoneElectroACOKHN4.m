estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[450]
    for numeroDeHormigas=[350]
        for rho=.26
            %             for alpha=1:5
            %                 for beta=1:5
            for alpha=[1.25]
                for Q=3
                    for ejecucion=1:20
                        for pheromoneFactor=[27.5 32.5]
                            for initialPheromone=[.1 3]
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,3,alpha,0,inf,'ordered roulette',2,3,31,pheromoneFactor);
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_workin1MMAS_reset'  '.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc
                            end
                        end
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
