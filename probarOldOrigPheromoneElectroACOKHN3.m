estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[350]
    for numeroDeHormigas=[400]
        for rho=[ .26 .3]
            for pheromoneInitial=.1
%                 for pheromoneFactor=[32.5 27.5]
                    %             for alpha=1:5
                    %                 for beta=1:5
                    for alpha=[.5 .9 1 1.1 1.25 1.3 1.5]
                        for Q=3
                            for ejecucion=1:15;
                                
                                tic
                                
                                [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACO(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),pheromoneInitial,Q,alpha,0,inf,'ordered roulette',2,3,3)
                                linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones pheromoneInitial]);
                                linea=[linea '\n'];
                                archivo=fopen(['KHN_informe_probar_alpha'  '.txt'],'a');
                                fprintf(archivo,linea);
                                fclose(archivo);
                                
                                toc
                                
                            end
                        end
                    end
                end
            end
        end
    end
% end

toc
format shortg
c = clock
