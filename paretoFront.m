function [paretoRoutes, bestG,bestWp,bestQp,bestSens1,bestSens2]= paretoFront( antsSolutions,objectivesArray,bestG,bestWp,bestQp,bestSens1,bestSens2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[antsNumber,~]=size(antsSolutions);
bestGIndexes=zeros(1,antsNumber);
bestWpIndexes=zeros(1,antsNumber);
bestQpIndexes=zeros(1,antsNumber);
bestSens1Indexes=zeros(1,antsNumber);
bestSens2Indexes=zeros(1,antsNumber);

for ant=1:antsNumber
    antSolution=antsSolutions(ant,:);
    [gErrorAux,wpErrorAux,qpErrorAux,~,sens1Aux,sens2Aux]=fitnessBaseOperations(antSolution,objectivesArray);
    if gErrorAux<bestG
        bestG=gErrorAux;
        bestGIndexes=zeros(1,antsNumber);
        bestGIndexes(ant)=1;
    elseif gError==bestG
        bestGIndexes(ant)=1;
    end
    if wpErrorAux<bestWp
        bestWp=gErrorAux;
        bestWpIndexes=zeros(1,antsNumber);
        bestWpIndexes(ant)=1;
    elseif gError==bestWp
        bestWpIndexes(ant)=1;
    end
    if qpErrorAux<bestQp
        bestQp=gErrorAux;
        bestQpIndexes=zeros(1,antsNumber);
        bestQpIndexes(ant)=1;
    elseif gError==bestQp
        bestQpIndexes(ant)=1;
    end
    if sens1Aux<bestSens1
        bestSens1=gErrorAux;
        bestSens1Indexes=zeros(1,antsNumber);
        bestSens1Indexes(ant)=1;
    elseif gError==bestSens1
        bestSens1Indexes(ant)=1;
    end
    if sens2Aux<bestSens2
        bestSens2=gErrorAux;
        bestSens2Indexes=zeros(1,antsNumber);
        bestSens2Indexes(ant)=1;
    elseif gError==bestSens2
        bestSens2Indexes(ant)=1;
    end
end
paretoFrontIndexes=unique([find(bestGIndexes) find(bestWpIndexes) find(bestQpIndexes)]);
paretoRoutes=antsSolutions(paretoFrontIndexes,:);
end

