function fitness=tenkaFilterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
% fitness=0;
% if gError>maxError||qpError>maxError||wpError>maxError
% %     gDiff=1;
% %     qpDiff=1;
% %     wpDiff=1;
% %     if gError>maxError
% %         gDiff=1+(gError-maxError)/maxError;
% %     end
% %     if qpError>maxError
% %         qpDiff=1+(qpError-maxError)/maxError;
% %     end
% %     if wpError>maxError
% %         wpDiff=1+(qpError-maxError)/maxError;
% %     end
% reference=min([gError qpError wpError]);
% gAddedCost=abs(gError-reference);
% qpAddedCost=abs(qpError-reference);
% wpAddedCost=abs(wpError-reference);
% fitness=gAddedCost+gError+qpAddedCost+qpError+wpAddedCost+wpError;
% fitness=1/fitness;
% %     fitness=[gDiff qpDiff wpDiff]*[gError qpError wpError];
% else
%     %fitness=sens1+sens2;
%     fitness=ones(3,1)'*([gError wpError 1.1*qpError])';
%     fitness=1/fitness;
% end

fitness=sum([gError wpError qpError])*(1+sens1)*(1+sens2);
fitness=1/fitness;
end