estadisticasContainer=containers.Map;
tic
win=randperm(100);
% win=win(1);
win=1002;


Q=3;
for numeroDeHormigas=[150 ];
numeroDeIteraciones=1300;
for mutationRate=[1.1 ];
for initialPheromone=[.007];
for pheromoneFactor=[ 1.9 ];
for rho=[ .38 ];
metodoDeSeleccion='uniform';
escenario=2;
for alpha=[.945]
for funcionDeCosto=[13] 
for ejecucion=1:1
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR12(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,Q,alpha,0,5,metodoDeSeleccion,escenario,funcionDeCosto,win,pheromoneFactor,mutationRate)
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor funcionDeCosto mutationRate win]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN2_sensibilidad_R12'  '_68s.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc

end
end
end
end
end
end
end
end

win
toc
format shortg
c = clock
