function [bestPath,bestCost,gError,wpError,qpError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR12(dimensionsNumber,rho,antsNumber,iterationsNumber,g,wp,qp,initialPheromoneConstant,Q,alpha,beta,errorPercentage,modoSeleccion,componentsMode,fitnessMode,figureIndex,pheromoneFactor,mutationRate)
%
%% import auxiliar functions
tic
addpath electroACOFramework;
%% startup
pheromoneConstant=initialPheromoneConstant;
antsSolutions=zeros(antsNumber,dimensionsNumber);
% maxIterationsNumberWithoutImprovement=40;
% maxIterationsWithoutReset=35;
% maxIterationsWithoutStuck=38;

vidas=1%3;
maxIterationsNumberWithoutImprovement=1000%8;
maxIterationsWithoutReset=15%3;
maxIterationsWithoutStuck=inf;%7;
iteracionesMaximasYaHabiendoEncontradoUnaBuena=1500;
contadorDeYaEncontramosUnaBuena=0;

minimumPheromone=initialPheromoneConstant/pheromoneFactor;
maximumPheromone=initialPheromoneConstant*pheromoneFactor;
bestPath=[];
resetCounter=0;
reseted=false;
programStuckedCounter=0;
softReset=false;

switch componentsMode
    case 1
        resistorsSerie='E96';
        capacitorsSerie='E24';
    case 2
        resistorsSerie='E24';
        capacitorsSerie='E12';
end

resistors=resistorCommercialValues(resistorsSerie);
capacitors=capacitorCommercialValues(capacitorsSerie);
capacitorsNumber=length(capacitors);
resistorsNumber=length(resistors);
% dimensionsElements=zeros(dimensionsNumber,max([capacitorsNumber resistorsNumber]));
dimensionsElements=cell(dimensionsNumber,1);

switch fitnessMode
    case 1
        fitnessFunction='euclideanFilterFitness';
    case 2
        fitnessFunction='filterFitness';
    case 3
        fitnessFunction='edoSenseiNoFilterFitness';
    case 4
        fitnessFunction='ameNoFilterFitness';
    case 5
        fitnessFunction='tenkaFilterFitness';
    case 9
        fitnessFunction='filterFitness9';
    case 10
        fitnessFunction='filterFitness10';
    case 11
        fitnessFunction='filterFitness11';
    case 13
        fitnessFunction='filterFitness13';
    case 14
        fitnessFunction='filterFitness14';
    case 15
        fitnessFunction='filterFitness15';
end

% objectivesArray=[g wp qp errorPercentage];
objectivesArray=[g wp qp inf];

%% initial random solution and dimensions setup

dimensionsSizes=zeros(1,dimensionsNumber);
%
% for dimensionIndex=1:6
%     dimensionsElements(dimensionIndex,1:resistorsNumber)=resistors;
%     dimensionsSizes(dimensionIndex)=resistorsNumber;
% end
% for dimensionIndex=7:8
%     dimensionsElements(dimensionIndex,1:capacitorsNumber)=capacitors;
%     dimensionsSizes(dimensionIndex)=capacitorsNumber;
% end

for dimensionIndex=1:6
    dimensionsElements{dimensionIndex}=resistors;
    dimensionsSizes(dimensionIndex)=resistorsNumber;
end
for dimensionIndex=7:8
    dimensionsElements{dimensionIndex}=capacitors;
    dimensionsSizes(dimensionIndex)=capacitorsNumber;
end

pheromones=buildPheromonesStructure(dimensionsSizes);
bestCost=inf;
%% iterate
bestCostsArray=zeros(1,iterationsNumber);
gErrorArray=zeros(1,iterationsNumber);
wpErrorArray=zeros(1,iterationsNumber);
qpErrorArray=zeros(1,iterationsNumber);
sens1Array=zeros(1,iterationsNumber);
sens2Array=zeros(1,iterationsNumber);
bestAntArray=zeros(1,iterationsNumber);
rawFitnessArray=zeros(1,iterationsNumber);
pheromoneConstantArray=zeros(1,iterationsNumber);
antsThatFollowedTheBestPathArray=zeros(1,iterationsNumber);
antsThatFollowedIterationBestPathArray=zeros(1,iterationsNumber);
costsArray=zeros(1,iterationsNumber);
visitedNodesArray=zeros(iterationsNumber,dimensionsNumber,resistorsNumber);
worstCostsArray=zeros(1,iterationsNumber);
averageCostsArray=zeros(1,iterationsNumber);
costsStdArray=zeros(1,iterationsNumber);
antsThatFollowedIterationWorstPathArray=zeros(1,iterationsNumber);
mutationsNumberArray=zeros(1,iterationsNumber);


noImprovementCounter=0;
iteration=1;

toc
tic
while noImprovementCounter<=maxIterationsNumberWithoutImprovement && iteration<=iterationsNumber && contadorDeYaEncontramosUnaBuena<iteracionesMaximasYaHabiendoEncontradoUnaBuena
    %% the main core of the metaheuristic
    iteration=iteration+1
    for antIndex=1:antsNumber
        currentAntSolution=antsSolutions(antIndex,:);
        for dimensionIndex=1:dimensionsNumber
            %             currentDimensionElements=nonzeros(dimensionsElements(dimensionIndex,:));
            currentDimensionElements=dimensionsElements{dimensionIndex};
            transitionProbabilities=ASTransitionProbability( alpha,beta,dimensionsElements,0,dimensionIndex,currentAntSolution,pheromones,pheromoneConstant);
            nextNodeIndex=selectionMethod(modoSeleccion,transitionProbabilities);
            currentAntSolution(dimensionIndex)=currentDimensionElements(nextNodeIndex);
        end
        antsSolutions(antIndex,:)=currentAntSolution;
    end
    
    [route,cost,ant]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    [worstRoute,worstCost,worstAnt]=selectWorstPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    [gErrorAux,wpErrorAux,qpErrorAux,~,sens1Aux,sens2Aux]=fitnessBaseOperations(route,objectivesArray);
    
    currentIterationCosts=zeros(1,antsNumber);
    accumFitness=0;
    for antIndex7=1:antsNumber
        antSolution=antsSolutions(antIndex7,:);
        currentIterationCosts(antIndex7)=feval(fitnessFunction,antSolution,objectivesArray);
        accumFitness=accumFitness+1/currentIterationCosts(antIndex7);
    end
    currentIterationStd=std(currentIterationCosts);
    averageFitness=(accumFitness/antsNumber) ;
    %% stop conditions (they do not imply a behavior modification)
    if cost<bestCost
        %% compare best result against best iteration result
        cost
        noImprovementCounter=0;
        resetCounter=0;
        programStuckedCounter=0;
        bestPath=route;
        bestCost=cost;
        bestAnt=ant;
        if contadorDeYaEncontramosUnaBuena>0
            contadorDeYaEncontramosUnaBuena=1;
        end
    elseif iteration-1>1
        programStuckedCounter=programStuckedCounter+1
        if bestCost==bestCostsArray(iteration-1)
            noImprovementCounter=noImprovementCounter+1;
            
            if softReset
                %                 resetCounter=resetCounter+1;
            end
        else
            noImprovementCounter=0;
        end
        if cost==costsArray(iteration-1)
            resetCounter=resetCounter+1
            if sum([gError qpError wpError]>4.99999)==0&&sum([sens1 sens2]>0.49999)==0
                contadorDeYaEncontramosUnaBuena=contadorDeYaEncontramosUnaBuena+1;
            end
        else
            resetCounter=0;
        end
    end
    if reseted&&programStuckedCounter>=maxIterationsWithoutStuck
        resetCounter=maxIterationsWithoutReset;
    end
    if~reseted&&noImprovementCounter>.59*maxIterationsNumberWithoutImprovement
        resetCounter=maxIterationsWithoutReset;
    end
    if resetCounter<maxIterationsWithoutReset%||programStuckedCounter<15
        %% do not use pheromone model if the algorith is upon to be reseted
        pheromones=MMASupdatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray,minimumPheromone,maximumPheromone);
        [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho);
        if pheromoneConstant<=minimumPheromone
            pheromoneConstant=minimumPheromone;
        end
        if reseted
            pheromones=MMASupdatePheromones(dimensionsElements,pheromones,bestPath,fitnessFunction,Q,pheromoneConstant,1,objectivesArray,minimumPheromone,maximumPheromone);
        end
        [currentIterationMutations,pheromones]=mutation(mutationRate,pheromones,minimumPheromone,maximumPheromone,dimensionsSizes);
        
    else
        'reinicio'
        softReset=true;
        %         if reseted<=1
        %             reseted=0;
        %         else
        %             reseted=reseted+1;
        %         end
        if softReset
            %% restart the algorithm write pheromones on best path
            softReset=false;
            reseted=true;
            resetCounter=0;
            noImprovementCounter=0;
            pheromones=buildPheromonesStructure(dimensionsSizes);
            programStuckedCounter=0;
        else
            softReset=true;
            resetCounter=0;
        end
        %         maxIterationsWithoutReset=maxIterationsWithoutReset;
        pheromoneConstant=initialPheromoneConstant;
        %         maximumPheromone=maximumPheromone/.9;
        %         minimumPheromone=minimumPheromone/1.1;
        programStuckedCounter=0;
%         pheromones=MMASupdatePheromones(dimensionsElements,pheromones,bestPath,fitnessFunction,Q,pheromoneConstant,1,objectivesArray,minimumPheromone,maximumPheromone);
        %         [pheromones,~]=evaporatePheromones(pheromones,pheromoneConstant,rho);
    end
    %     if programStuckedCounter>15
    %         maximumPheromone=maximumPheromone/.9;
    %         minimumPheromone=minimumPheromone/1.1;
    %         programStuckedCounter=0;
    %     end
    [gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(bestPath,objectivesArray);
    
    
    %         %% 29/05 01:37 those solutions having an higher error than the best solution will not add pheromone
    %             objectivesArray(4)=max([gError wpError qpError]);
    %             errorPercentage=objectivesArray(4);
    
    %% fill arrays to be plotted
    fillArraysToBePlotted;
    
    %         for dimension=1:dimensionsNumber
    %             pheromones=acceleratedPheromonesUpdate(dimension,dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,bestAnt,objectivesArray,rho);
    %         end
    %% salvavidas
    if vidas>1 && iteration>.75*iterationsNumber &&contadorDeYaEncontramosUnaBuena==0
        vidas=vidas-1;
        contadorDeYaEncontramosUnaBuena=0;
        minimumPheromone=initialPheromoneConstant/pheromoneFactor;
        maximumPheromone=initialPheromoneConstant*pheromoneFactor;
        bestPath=[];
        resetCounter=0;
        reseted=false;
        programStuckedCounter=0;
        softReset=false;
        pheromones=buildPheromonesStructure(dimensionsSizes);
        bestCost=inf;
        noImprovementCounter=0;
        iteration=1;
    end
    
end
vidas
toc
plotResults;

%% auxiliar functions

    function fillArraysToBePlotted
        antsThatFollowedTheBestPathOnCurrentIteration=sum(ismember(antsSolutions,bestPath,'rows'));
        antsThatFollowedIterationBestPath=sum(ismember(antsSolutions,route,'rows'));
        antsThatFollowedIterationWorstPath=sum(ismember(antsSolutions,worstRoute,'rows'));
        bestCostsArray(iteration)=bestCost;
        costsArray(iteration)=cost;
        gErrorArray(iteration)=gError;
        wpErrorArray(iteration)=wpError;
        qpErrorArray(iteration)=qpError;
        bestAntArray(iteration)=bestAnt;
        rawFitnessArray(iteration)=cost;
        pheromoneConstantArray(iteration)=pheromoneConstant;
        antsThatFollowedTheBestPathArray(iteration)=antsThatFollowedTheBestPathOnCurrentIteration;
        antsThatFollowedIterationBestPathArray(iteration)=antsThatFollowedIterationBestPath;
        sens1Array(iteration)=sens1;
        sens2Array(iteration)=sens2;
        worstCostsArray(iteration)=worstCost;
        averageCostsArray(iteration)=averageFitness;
        costsStdArray(iteration)=currentIterationStd;
        antsThatFollowedIterationWorstPathArray(iteration)=antsThatFollowedIterationWorstPath;
        mutationsNumberArray(iteration)=currentIterationMutations;
    end

    function plotResults
        %% plot results
        bestCostsArray((iteration):end)=[];
        worstCostsArray((iteration):end)=[];
        averageCostsArray((iteration):end)=[];
        gErrorArray((iteration):end)=[];
        wpErrorArray((iteration):end)=[];
        qpErrorArray((iteration):end)=[];
        sens1Array((iteration):end)=[];
        sens2Array((iteration):end)=[];
        bestAntArray((iteration):end)=[];
        rawFitnessArray((iteration):end)=[];
        pheromoneConstantArray((iteration):end)=[];
        antsThatFollowedTheBestPathArray((iteration):end)=[];
        antsThatFollowedIterationBestPathArray((iteration):end)=[];
        costsStdArray((iteration):end)=[];
        antsThatFollowedIterationWorstPathArray((iteration):end)=[];
        mutationsNumberArray((iteration):end)=[];
        
        % [bestPath,bestCost,~]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
        figure(figureIndex)
        subplot(3,5,1);
        
        plot(bestCostsArray);
        title('costo de s*');
        subplot(3,5,2);
        
        plot(gErrorArray);
        title('gError de s*');
        subplot(3,5,3);
        
        plot(wpErrorArray);
        title('wpError de s*');
        subplot(3,5,4);
        
        plot(qpErrorArray);
        title('qpError de s*');
        subplot(3,5,5);
        
        % plot(bestAntArray);
        % title('bestAnt');
        
        plot(antsThatFollowedIterationBestPathArray);
        title('numero de hormigas que siguieron s*i');
        
        subplot(3,5,6);
        plot(pheromoneConstantArray);
        title('constante de feromonas');
        
        subplot(3,5,7);
        plot(rawFitnessArray);
        title('costo de s*i');
        
        
        subplot(3,5,8);
        plot(antsThatFollowedTheBestPathArray);
        title('hormigas que siguieron s*');
        
        subplot(3,5,9);
        plot(sens1Array);
        title('sens1 de s*');
        
        subplot(3,5,10);
        plot(sens2Array);
        title('sens2 de s*');
        
        subplot(3,5,11);
        plot(worstCostsArray);
        title('peor costo');
        
        subplot(3,5,12);
        plot(averageCostsArray);
        title('costo promedio');
        
        subplot(3,5,13);
        plot(costsStdArray);
        title('desviacion std del costo');
        
        subplot(3,5,14);
        plot(antsThatFollowedIterationWorstPathArray);
        title('numero de hormigas que siguieron w*i');
        
        subplot(3,5,15);
        plot(mutationsNumberArray);
        title('mutaciones');
        
        %         figure(figureIndex+1);
        %         offset=2; hold on;
        %         for i=1:40
        % %             surf(visitedNodesArray(i,:,:)+offset*(i-1));
        %         end
    end

end

