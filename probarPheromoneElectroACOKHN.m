estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=30
    for numeroDeHormigas=30
        for rho=[.1 .3 .5 .7 .9]
            %             for alpha=1:5
            %                 for beta=1:5
            for alpha=[.5 1 2 4]
                for Q=3
                    for errorPercentage=60
                        
                        
                        for ejecucion=1:5
                            tic
                            [path,cost,gError,wpError,qError,sens1,sens2]=pheromoneKHNACO(8,.9,30,30,3,2000*pi,1/sqrt(2),.1,3,alpha,0,errorPercentage,'uniform',2,3);
                            linea=num2str([path,cost,gError,wpError,qError,sens1,sens2 Q rho numeroDeIteraciones numeroDeHormigas errorPercentage]);
                            linea=[linea '\n'];
                            archivo=fopen(['KHN_pheromone_prueba_1'  'ma.txt'],'a');
                            fprintf(archivo,linea);
                            fclose(archivo);
                            toc
                        end
                        
                        
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
