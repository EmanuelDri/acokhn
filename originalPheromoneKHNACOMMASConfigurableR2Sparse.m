function [bestPath,bestCost,gError,wpError,qpError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR2Sparse(dimensionsNumber,rho,antsNumber,iterationsNumber,g,wp,qp,initialPheromoneConstant,Q,alpha,beta,errorPercentage,modoSeleccion,componentsMode,fitnessMode,figureIndex,pheromoneFactor)
%
%% import auxiliar functions
tic
addpath electroACOFramework;
%% startup
pheromoneConstant=initialPheromoneConstant;
antsSolutions=zeros(antsNumber,dimensionsNumber);
maxIterationsNumberWithoutImprovement=75;
maxIterationsWithoutReset=20;
maxIterationsWithoutStuck=155;
minimumPheromone=initialPheromoneConstant/pheromoneFactor;
maximumPheromone=initialPheromoneConstant*pheromoneFactor;
bestPath=[];
resetCounter=0;
reseted=false;
programStuckedCounter=0;
softReset=false;

switch componentsMode
    case 1
        resistorsSerie='E96';
        capacitorsSerie='E24';
    case 2
        resistorsSerie='E24';
        capacitorsSerie='E12';
end

resistors=resistorCommercialValues(resistorsSerie);
capacitors=capacitorCommercialValues(capacitorsSerie);
capacitorsNumber=length(capacitors);
resistorsNumber=length(resistors);
dimensionsElements=zeros(dimensionsNumber,max([capacitorsNumber resistorsNumber]));

switch fitnessMode
    case 1
        fitnessFunction='euclideanFilterFitness';
    case 2
        fitnessFunction='filterFitness';
    case 3
        fitnessFunction='edoSenseiNoFilterFitness';
    case 4
        fitnessFunction='ameNoFilterFitness';
    case 5
        fitnessFunction='tenkaFilterFitness';
end

% objectivesArray=[g wp qp errorPercentage];
objectivesArray=[g wp qp inf];

%% initial random solution and dimensions setup

dimensionsSizes=zeros(1,dimensionsNumber);

for dimensionIndex=1:6
    dimensionsElements(dimensionIndex,1:resistorsNumber)=resistors;
    dimensionsSizes(dimensionIndex)=resistorsNumber;
end
for dimensionIndex=7:8
    dimensionsElements(dimensionIndex,1:capacitorsNumber)=capacitors;
    dimensionsSizes(dimensionIndex)=capacitorsNumber;
end

pheromones=buildPheromonesStructureSparse(dimensionsSizes);
bestCost=inf;
%% iterate
bestCostsArray=zeros(1,iterationsNumber);
gErrorArray=zeros(1,iterationsNumber);
wpErrorArray=zeros(1,iterationsNumber);
qpErrorArray=zeros(1,iterationsNumber);
sens1Array=zeros(1,iterationsNumber);
sens2Array=zeros(1,iterationsNumber);
bestAntArray=zeros(1,iterationsNumber);
rawFitnessArray=zeros(1,iterationsNumber);
pheromoneConstantArray=zeros(1,iterationsNumber);
antsThatFollowedTheBestPathArray=zeros(1,iterationsNumber);
antsThatFollowedIterationBestPathArray=zeros(1,iterationsNumber);
costsArray=zeros(1,iterationsNumber);
visitedNodesArray=zeros(iterationsNumber,dimensionsNumber,resistorsNumber);

noImprovementCounter=0;
iteration=1;

toc
tic
while noImprovementCounter<=maxIterationsNumberWithoutImprovement && iteration<iterationsNumber %&& programStuckedCounter<maxIterationsWithoutStuck
    %% the main core of the metaheuristic
    
    for antIndex=1:antsNumber
        currentAntSolution=antsSolutions(antIndex,:);
        for dimensionIndex=1:dimensionsNumber
            currentDimensionElements=nonzeros(dimensionsElements(dimensionIndex,:));
            transitionProbabilities=ASTransitionProbabilitySparse( alpha,beta,dimensionsElements,0,dimensionIndex,currentAntSolution,pheromones,pheromoneConstant,resistorsNumber);
            nextNodeIndex=selectionMethod(modoSeleccion,transitionProbabilities);
            currentAntSolution(dimensionIndex)=currentDimensionElements(nextNodeIndex);
        end
        antsSolutions(antIndex,:)=currentAntSolution;
    end
    
    [route,cost,ant]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    [gErrorAux,wpErrorAux,qpErrorAux,~,sens1Aux,sens2Aux]=fitnessBaseOperations(route,objectivesArray);
    
    %% stop conditions (they do not imply a behavior modification)
    if cost<bestCost
        %% compare best result against best iteration result
        cost
        noImprovementCounter=0;
        resetCounter=0;
        programStuckedCounter=0;
        bestPath=route;
        bestCost=cost;
        bestAnt=ant;
    elseif iteration-1>1
        if bestCost==bestCostsArray(iteration-1)
            noImprovementCounter=noImprovementCounter+1
            programStuckedCounter=programStuckedCounter+1;
            if softReset
%                 resetCounter=resetCounter+1;
            end
        else
            noImprovementCounter=0;
        end
        if (cost==costsArray(iteration-1))%||bestCost==bestCostsArray(iteration-1)
            resetCounter=resetCounter+1
        else
            resetCounter=0;
        end
    end
    
    if resetCounter<maxIterationsWithoutReset%||programStuckedCounter<15
        %% do not use pheromone model if the algorith is upon to be reseted
        pheromones=MMASupdatePheromonesSparse(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray,minimumPheromone,maximumPheromone);
        [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho);
        if pheromoneConstant<=minimumPheromone
            pheromoneConstant=minimumPheromone;
        end
% %         if reseted
% if true
%             pheromones=MMASupdatePheromonesSparse(dimensionsElements,pheromones,bestPath,fitnessFunction,Q,pheromoneConstant,1,objectivesArray,minimumPheromone,maximumPheromone);
%         end
        
    else
        softReset=true;
        if softReset
            %% restart the algorithm write pheromones on best path
            softReset=false;
            reseted=true;
            resetCounter=0;
            noImprovementCounter=0;
            pheromones=buildPheromonesStructureSparse(dimensionsSizes);
            programStuckedCounter=0;
        else
            softReset=true;
            resetCounter=0;
        end
        %         maxIterationsWithoutReset=maxIterationsWithoutReset;
%          if sum([gError wpError qpError]<=errorPercentage)>1
%             fitnessFunction='filterFitness';
%         else
%             fitnessFunction='edoSenseiNoFilterFitness';
%         end
        pheromoneConstant=initialPheromoneConstant;
        maximumPheromone=maximumPheromone/.9;
        minimumPheromone=minimumPheromone/1.1;
        programStuckedCounter=0;
        pheromones=MMASupdatePheromonesSparse(dimensionsElements,pheromones,bestPath,fitnessFunction,Q,pheromoneConstant,1,objectivesArray,minimumPheromone,maximumPheromone);
    end
%     if programStuckedCounter>15
%         maximumPheromone=maximumPheromone/.9;
%         minimumPheromone=minimumPheromone/1.1;
%         programStuckedCounter=0;
%     end
    [gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(bestPath,objectivesArray);
    
       
    
    %         %% 29/05 01:37 those solutions having an higher error than the best solution will not add pheromone
    %             objectivesArray(4)=max([gError wpError qpError]);
    %             errorPercentage=objectivesArray(4);
    
    %% fill arrays to be plotted
    fillArraysToBePlotted;
    
    %         for dimension=1:dimensionsNumber
    %             pheromones=acceleratedPheromonesUpdate(dimension,dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,bestAnt,objectivesArray,rho);
    %         end
    iteration=iteration+1
end
toc
plotResults;

%% auxiliar functions

     function fillArraysToBePlotted
        antsThatFollowedTheBestPathOnCurrentIteration=sum(ismember(antsSolutions,bestPath,'rows'));
        antsThatFollowedIterationBestPath=sum(ismember(antsSolutions,route,'rows'));
        bestCostsArray(iteration)=bestCost;
        costsArray(iteration)=cost;
        gErrorArray(iteration)=gError;
        wpErrorArray(iteration)=wpError;
        qpErrorArray(iteration)=qpError;
        bestAntArray(iteration)=bestAnt;
        rawFitnessArray(iteration)=cost;
        pheromoneConstantArray(iteration)=pheromoneConstant;
        antsThatFollowedTheBestPathArray(iteration)=antsThatFollowedTheBestPathOnCurrentIteration;
        antsThatFollowedIterationBestPathArray(iteration)=antsThatFollowedIterationBestPath;
        sens1Array(iteration)=sens1;
        sens2Array(iteration)=sens2;
        for dimensionAuxiliarIndex=1:8
            actualDimensionElements=squeeze(dimensionsElements(dimensionAuxiliarIndex,:));
            for antAuxiliarIndex=1:antsNumber
                if ~ismember(antsSolutions(antAuxiliarIndex,dimensionAuxiliarIndex),visitedNodesArray(iteration,dimensionAuxiliarIndex,:))
                 nodeIndex= antsSolutions(antAuxiliarIndex,dimensionAuxiliarIndex)==actualDimensionElements;   
                 visitedNodesArray(iteration,dimensionAuxiliarIndex,nodeIndex)=1;
                end
            end
        end
    end

    function plotResults
        %% plot results
          bestCostsArray=nonzeros(bestCostsArray);
        gErrorArray=nonzeros(gErrorArray);
        wpErrorArray=nonzeros(wpErrorArray);
        qpErrorArray=nonzeros(qpErrorArray);
        sens1Array=nonzeros(sens1Array);
        sens2Array=nonzeros(sens2Array);
        bestAntArray=nonzeros(bestAntArray);
        rawFitnessArray=nonzeros(rawFitnessArray);
        pheromoneConstantArray=nonzeros(pheromoneConstantArray);
        antsThatFollowedTheBestPathArray=nonzeros(antsThatFollowedTheBestPathArray);
        antsThatFollowedIterationBestPathArray=nonzeros(antsThatFollowedIterationBestPathArray);
        
        % [bestPath,bestCost,~]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
        figure(figureIndex)
        subplot(2,5,1);
        
        plot(bestCostsArray);
        title('costo de la s*');
        subplot(2,5,2);
        
        plot(gErrorArray);
        title('gError de s*');
        subplot(2,5,3);
        
        plot(wpErrorArray);
        title('wpError de s*');
        subplot(2,5,4);
        
        plot(qpErrorArray);
        title('qpError de s*');
        subplot(2,5,5);
        
        % plot(bestAntArray);
        % title('bestAnt');
        
        plot(antsThatFollowedIterationBestPathArray);
        title('numero de hormigas que siguieron s*i');
        
        subplot(2,5,6);
        plot(pheromoneConstantArray);
        title('constante de feromonas');
        
        subplot(2,5,7);
        plot(rawFitnessArray);
        title('costo de s*i');
        
        
        subplot(2,5,8);
        plot(antsThatFollowedTheBestPathArray);
        title('hormigas que siguieron s*');
        
        subplot(2,5,9);
        plot(sens1Array);
        title('sens1 de s*');
        
        subplot(2,5,10);
        plot(sens2Array);
        title('sens2 de s*');
        
%         figure(figureIndex+1);
%         offset=2; hold on; 
%         for i=1:40
% %             surf(visitedNodesArray(i,:,:)+offset*(i-1));
%         end
    end

end

