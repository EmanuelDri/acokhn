estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[250]
    for numeroDeHormigas=[400]
        for rho=[.26]
            for pheromoneInitial= [   0.1 ]
%                 for pheromoneFactor=[32.5 27.5]
                    %             for alpha=1:5
                    %                 for beta=1:5
                    for alpha=[ 1.25 ]
                        for Q=3
                            for ventana=32.5
                            for ejecucion=1:3;
                                
                                tic
                                
                                [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR11(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),pheromoneInitial,Q,alpha,0,inf,'ordered roulette',2,3,8,ventana)
                                linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones pheromoneInitial ventana]);
                                linea=[linea '\n'];
                                archivo=fopen(['KHN_informe_probar_MMAS_fR11'  '.txt'],'a');
                                fprintf(archivo,linea);
                                fclose(archivo);
                                
                                toc
                            end
                            end
                        end
                    end
                end
            end
        end
    end
% end

toc
format shortg
c = clock
