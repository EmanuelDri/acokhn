function [bestPath,bestCost,gError,wpError,qpError,sens1,sens2]=originalPheromoneKHNACOFullMMAS(dimensionsNumber,rho,antsNumber,iterationsNumber,g,wp,qp,initialPheromoneConstant,Q,alpha,beta,errorPercentage,modoSeleccion,componentsMode,fitnessMode,figureIndex,pheromoneFactor)
%
%% import auxiliar functions
tic
addpath electroACOFramework;
%% startup
pheromoneConstant=initialPheromoneConstant;
antsSolutions=zeros(antsNumber,dimensionsNumber);
dimensionsElements=cell(1,dimensionsNumber);
maxIterationsNumberWithoutImprovement=50;
minimumPheromone=initialPheromoneConstant/pheromoneFactor;
maximumPheromone=initialPheromoneConstant*pheromoneFactor;

switch componentsMode
    case 1
        resistorsSerie='E96';
        capacitorsSerie='E24';
    case 2
        resistorsSerie='E24';
        capacitorsSerie='E12';
end

resistors=resistorCommercialValues(resistorsSerie);
capacitors=capacitorCommercialValues(capacitorsSerie);
capacitorsNumber=length(capacitors);
resistorsNumber=length(resistors);

switch fitnessMode
    case 1
        fitnessFunction='euclideanFilterFitness';
    case 2
        fitnessFunction='filterFitness';
    case 3
        fitnessFunction='edoSenseiNoFilterFitness';
    case 4
        fitnessFunction='ameNoFilterFitness';
end

objectivesArray=[g wp qp errorPercentage];

%% initial random solution and dimensions setup

dimensionsSizes=zeros(1,dimensionsNumber);

for dimensionIndex=1:6
    dimensionsElements{dimensionIndex}=resistors;
    dimensionsSizes(dimensionIndex)=resistorsNumber;
end
for dimensionIndex=7:8
    dimensionsElements{dimensionIndex}=capacitors;
    dimensionsSizes(dimensionIndex)=capacitorsNumber;
end

pheromones=buildPheromonesStructure(dimensionsSizes);
bestCost=inf;
%% iterate
costsArray=zeros(1,iterationsNumber);
gErrorArray=zeros(1,iterationsNumber);
wpErrorArray=zeros(1,iterationsNumber);
qpErrorArray=zeros(1,iterationsNumber);
sens1Array=zeros(1,iterationsNumber);
sens2Array=zeros(1,iterationsNumber);
bestAntArray=zeros(1,iterationsNumber);
rawFitnessArray=zeros(1,iterationsNumber);
pheromoneConstantArray=zeros(1,iterationsNumber);
antsThatFollowedTheBestPathArray=zeros(1,iterationsNumber);
antsThatFollowedIterationBestPathArray=zeros(1,iterationsNumber);

noImprovementCounter=0;
iteration=1;

toc
tic
while noImprovementCounter<=maxIterationsNumberWithoutImprovement && iteration<=iterationsNumber
    %% the main core of the metaheuristic
    iteration=iteration+1
    for antIndex=1:antsNumber
        currentAntSolution=antsSolutions(antIndex,:);
        for dimensionIndex=1:dimensionsNumber
            currentDimensionElements=dimensionsElements{dimensionIndex};
            transitionProbabilities=ASTransitionProbability( alpha,beta,dimensionsElements,0,dimensionIndex,currentAntSolution,pheromones,pheromoneConstant);
            nextNodeIndex=selectionMethod(modoSeleccion,transitionProbabilities);
            currentAntSolution(dimensionIndex)=currentDimensionElements(nextNodeIndex);
        end
        antsSolutions(antIndex,:)=currentAntSolution;
    end
    
    [route,cost,iterationBestAnt]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    [gErrorAux,wpErrorAux,qpErrorAux,~,sens1Aux,sens2Aux]=fitnessBaseOperations(route,objectivesArray);
    
    %% stop conditions (they do not imply a behavior modification)
        if cost<=bestCost
            %% compare best result against best iteration result
            feasibleSolutionConditionForCurrentIteration=gErrorAux<=errorPercentage && wpErrorAux<=errorPercentage && qpErrorAux<=errorPercentage;
            if feasibleSolutionConditionForCurrentIteration && noImprovementCounter<=maxIterationsNumberWithoutImprovement && cost<bestCost
                cost
                noImprovementCounter=0;
            elseif feasibleSolutionConditionForCurrentIteration && cost==bestCost
                cost
                noImprovementCounter
                noImprovementCounter=noImprovementCounter+1;
            elseif ~isempty(bestPath)&& cost==bestCost
                [gErrorBestPath,wpErrorBestPath,qpErrorBestPath,~,~,~]=fitnessBaseOperations(route,objectivesArray);
                if gErrorBestPath<=errorPercentage && wpErrorBestPath<=errorPercentage && qpErrorBestPath <=errorPercentage
                    noImprovementCounter=noImprovementCounter+1;
                end
            end
            bestPath=route;
            bestCost=cost;
            bestAnt=iterationBestAnt;
        elseif iteration-1>1&& bestCost==costsArray(iteration-1)
            noImprovementCounter=noImprovementCounter+1;
        end

        [gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(bestPath,objectivesArray);
               
        if gError<=errorPercentage && wpError<=errorPercentage && qpError<=errorPercentage
            %% do not use pheromone model unless a good solution has been found
%MMAS elitist pheromone update
            pheromones=MMASupdatePheromones(dimensionsElements,pheromones,route,fitnessFunction,Q,pheromoneConstant,1,objectivesArray,minimumPheromone,maximumPheromone);
            [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho);
            if pheromoneConstant<=minimumPheromone
                pheromoneConstant=minimumPheromone;
            end

        end
%         %% 29/05 01:37 those solutions having an higher error than the best solution will not add pheromone
%             objectivesArray(4)=max([gError wpError qpError]);
%             errorPercentage=objectivesArray(4);
        
     %% fill arrays to be plotted
       fillArraysToBePlotted;

%         
end
toc
plotResults;

%% auxiliar functions

    function fillArraysToBePlotted
         antsThatFollowedTheBestPathOnCurrentIteration=sum(ismember(antsSolutions,bestPath,'rows'));
        antsThatFollowedIterationBestPath=sum(ismember(antsSolutions,route,'rows'));


        costsArray(iteration)=bestCost;
        gErrorArray(iteration)=gError;
        wpErrorArray(iteration)=wpError;
        qpErrorArray(iteration)=qpError;
        bestAntArray(iteration)=bestAnt;
        rawFitnessArray(iteration)=cost;
        pheromoneConstantArray(iteration)=pheromoneConstant;
        antsThatFollowedTheBestPathArray(iteration)=antsThatFollowedTheBestPathOnCurrentIteration;
        antsThatFollowedIterationBestPathArray(iteration)=antsThatFollowedIterationBestPath;
    end

    function plotResults
%% plot results
    costsArray((iteration+1):end)=[];
    gErrorArray((iteration+1):end)=[];
    wpErrorArray((iteration+1):end)=[];
    qpErrorArray((iteration+1):end)=[];
    sens1Array((iteration+1):end)=[];
    sens2Array((iteration+1):end)=[];
    bestAntArray((iteration+1):end)=[];
    rawFitnessArray((iteration+1):end)=[];
    pheromoneConstantArray((iteration+1):end)=[];
    antsThatFollowedTheBestPathArray((iteration+1):end)=[];
    antsThatFollowedIterationBestPathArray((iteration+1):end)=[];

    % [bestPath,bestCost,~]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    figure(figureIndex)
    subplot(2,4,1);

    plot(costsArray);
    title('cost');
    subplot(2,4,2);

    plot(gErrorArray);
    title('gError');
    subplot(2,4,3);

    plot(wpErrorArray);
    title('wpError');
    subplot(2,4,4);

    plot(qpErrorArray);
    title('qpError');
    subplot(2,4,5);

    % plot(bestAntArray);
    % title('bestAnt');

    plot(antsThatFollowedIterationBestPathArray);
    title('ants that followed iteration best path');

    subplot(2,4,6);
    plot(pheromoneConstantArray);
    title('pheromoneConstant');

    subplot(2,4,7);
    plot(rawFitnessArray);
    title('iteration best fitness');

    subplot(2,4,8);
    plot(antsThatFollowedTheBestPathArray);
    title('ants that followed best path');
    end

end

