estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[450]
    for numeroDeHormigas=[400]
        for rho=.25
            %             for alpha=1:5
            %                 for beta=1:5
            for alpha=[1.25 1.5 2 2.5]
                for Q=3
                    for ejecucion=1:4;
                        
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACO(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),7,3,alpha,0,inf,'ordered roulette',2,3,5);
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones ejecucion]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_workin1MMAS_grace_time_100'  '.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc
                        
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
