function pheromones=acceleratedPheromonesUpdate(dimensionInd,dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antInd,objectivesArray,rho)
antSolution=antsSolutions(antInd,:);
element=antSolution(dimensionInd);
elementIndex(dimensionInd)=find(dimensionsElements{dimensionInd}==element);
cost=1/feval(fitnessFunction,antSolution,objectivesArray);
antDepostion=Q/cost;
dimensionPheromones=pheromones{dimensionInd};
switch dimensionInd
    case 1
        firstNodeIndex=elementIndex(dimensionInd);
        arcPheromone=dimensionPheromones{firstNodeIndex};
        if isempty(arcPheromone)
            dimensionPheromones{firstNodeIndex}=pheromoneConstant*(1-rho)+antDepostion;
        else
            dimensionPheromones{firstNodeIndex}=dimensionPheromones{firstNodeIndex}+antDepostion;
        end
    otherwise
        actualElementIndex=elementIndex(dimensionInd);
        previousElement=antSolution(dimensionInd-1);
        previousElementIndex=find(dimensionsElements{dimensionInd-1}==previousElement);
        arcPheromone=dimensionPheromones{previousElementIndex,actualElementIndex};
        if isempty(arcPheromone)
            dimensionPheromones{previousElementIndex,actualElementIndex}=pheromoneConstant+antDepostion;
        else
            dimensionPheromones{previousElementIndex,actualElementIndex}=dimensionPheromones{previousElementIndex,actualElementIndex}+antDepostion;
        end
end
pheromones{dimensionInd}=dimensionPheromones;
end

