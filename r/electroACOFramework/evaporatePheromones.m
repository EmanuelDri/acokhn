function [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho)
        dimensionsNumber=length(pheromones);
        for dimensionInd=1:dimensionsNumber
            dimensionCells=pheromones{dimensionInd};
            dimensionCells=cellfun(@(x) x*(1-rho),dimensionCells,'UniformOutput',false);
            pheromones{dimensionInd}=dimensionCells;
            pheromoneConstant=pheromoneConstant*(1-rho);
        end
    end