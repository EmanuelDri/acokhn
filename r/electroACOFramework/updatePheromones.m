function [pheromones]=updatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray)
%updatePheromones This function updates the pheromone amounts within arcs
% in a given graph
% dimensionsElements is a cell array that contains for each dimension its
% associated decision elements
dimensionsNumber=length(dimensionsElements);
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    elementsIndexes=zeros(1,dimensionsNumber);
    for dimension=1:dimensionsNumber
        element=antSolution(dimension);
        dimensionElements=dimensionsElements{dimension};
        elementsIndexes(dimension)=find(dimensionElements==element);
    end
    cost=1/feval(fitnessFunction,antSolution,objectivesArray);
    antDepostion=Q/cost;
    for dimensionInd=1:dimensionsNumber
        dimensionPheromones=pheromones{dimensionInd};
        if dimensionInd==1
            firstNodeIndex=elementsIndexes(dimensionInd);
            arcPheromone=dimensionPheromones{firstNodeIndex};
            if isempty(arcPheromone)
                dimensionPheromones{firstNodeIndex}=pheromoneConstant+antDepostion;
            else
                dimensionPheromones{firstNodeIndex}=dimensionPheromones{firstNodeIndex}+antDepostion;
            end
        else
            actualElementIndex=elementsIndexes(dimensionInd);
            previousElementIndex=elementsIndexes(dimensionInd-1);
            arcPheromone=dimensionPheromones{previousElementIndex,actualElementIndex};
            if isempty(arcPheromone)
                dimensionPheromones{previousElementIndex,actualElementIndex}=pheromoneConstant+antDepostion;
            else
                dimensionPheromones{previousElementIndex,actualElementIndex}=dimensionPheromones{previousElementIndex,actualElementIndex}+antDepostion;
            end
        end
        pheromones{dimensionInd}=dimensionPheromones;
    end
end
end