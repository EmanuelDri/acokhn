function [orderedVector,indexesBeforeOrdering] = order( vector,mode)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

switch mode
    case 'majorToMinor'
        operation='max';
        reference=0;
    case 'minorToMajor'
        operation='min';
        reference=inf;
end

orderedVector=zeros(1,length(vector));
indexesBeforeOrdering=orderedVector;
for index=1:length(vector)
    [element,pos]=feval(operation,vector);
    orderedVector(index)=element;
    vector(pos)=reference;
    indexesBeforeOrdering(index)=pos;
end