 function element=randomElement(elementsArray)
        valuesNumber=length(elementsArray);
        randomValue=1+floor(rand*valuesNumber);
        element=elementsArray(randomValue);
    end