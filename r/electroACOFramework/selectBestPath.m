function [route,cost,ant]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber)
cost=Inf;
ant=0;
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    adequacy=feval(fitnessFunction,antSolution,objectivesArray);
    adequacy=1/adequacy;       
    if adequacy<=cost
        cost=adequacy;
        route=antSolution;
		ant=antInd;
    end
    
end
end