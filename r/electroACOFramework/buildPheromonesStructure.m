function pheromones=buildPheromonesStructure(dimensionsSizes)
dimensionsNumber=length(dimensionsSizes);
        pheromones=cell(1,dimensionsNumber);
        for dimension=1:dimensionsNumber
            switch dimension
                case  1
                    pheromones{dimension}=cell(1,dimensionsSizes(1));
                otherwise
                    pheromones{dimension}=cell(dimensionsSizes(dimension-1),dimensionsSizes(dimension));
            end
        end
end
