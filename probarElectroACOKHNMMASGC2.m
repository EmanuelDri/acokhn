estadisticasContainer=containers.Map;
tic
win=randperm(100);
win=win(1);


Q=3;
for alpha=[1 1.25]
numeroDeHormigas=450;
numeroDeIteraciones=250;
initialPheromone=.1;
pheromoneFactor=32.5;
rho=.26;
metodoDeSeleccion='ordered roulette';
escenario=2;
for funcionDeCosto=2;

for ejecucion=1:3
                        tic
                        
                        [path,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR11(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,Q,alpha,0,inf,metodoDeSeleccion,escenario,funcionDeCosto,win,pheromoneFactor)
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_sensibilidad_R11_op2'  '_11.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc

end
end
end

toc
format shortg
c = clock
