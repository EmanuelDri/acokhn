function [path,bestMeanCost,ant]=selectMeanBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber)
%% selectMeanBestPath
% this function selects the best path based on the mean of solutions
% errors corresponding to variables G,ωp an Qp
cost=Inf;
bestMeanCost=inf;
ant=0;
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    [gError,wpError,qpError,~,~,~]=fitnessBaseOperations(antSolution,objectivesArray);
    currentMeanCost=mean([gError,wpError,qpError]);
    if currentMeanCost==bestMeanCost
        adequacy=feval(fitnessFunction,antSolution,objectivesArray);
        adequacy=1/adequacy;
        if adequacy<=cost
            cost=adequacy;
            path=antSolution;
            ant=antInd;
            bestMeanCost=currentMeanCost;
        end
    elseif currentMeanCost<=bestMeanCost
        path=antSolution;
        ant=antInd;
        bestMeanCost=currentMeanCost;
        adequacy=feval(fitnessFunction,antSolution,objectivesArray);
        adequacy=1/adequacy;
        cost=adequacy;
    end
    
end
end