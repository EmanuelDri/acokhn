function [mutationsPerformed,pheromoneModel] = mutation(mutationRate, pheromoneModel, minPheromone,maxPheromone, dimensionsSizes)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
dimensionsNumber=length(dimensionsSizes);
dimensionsSizes=[1 dimensionsSizes];
mutationsPerformed=0;
for dimension=1:dimensionsNumber
    previousDimensionSize=dimensionsSizes(dimension);
    currentDimensionSize=dimensionsSizes(dimension+1);
    currentDimensionMutations=ceil(currentDimensionSize*mutationRate*.01);
    previousDimensionMutations=ceil(previousDimensionSize*mutationRate*.01);
    currentDimensionArcsIndexes=unique(ceil(rand(1,currentDimensionMutations)*(currentDimensionSize-1)+1));
    for currentDimensionIndex=currentDimensionArcsIndexes
        previousDimensionArcsIndexes=(ceil(rand(1,previousDimensionMutations)*(previousDimensionSize-1)+1));
        for previousDimensionIndex=previousDimensionArcsIndexes
            if .001*mutationRate>=rand
                'mutation'
                mutationsPerformed=mutationsPerformed+1;
                currentMutationPheromone=rand*(maxPheromone-minPheromone)+minPheromone;
                pheromoneModel(dimension,previousDimensionIndex,currentDimensionIndex)=currentMutationPheromone;
            end
        end
        
    end
end

end

