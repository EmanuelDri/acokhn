function [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho)
%         dimensionsNumber=length(pheromones);
%         for dimensionInd=1:dimensionsNumber
%             dimensionCells=pheromones{dimensionInd};
%             dimensionCells=dimensionCells*(1-rho);
%             pheromones{dimensionInd}=dimensionCells;    
%         end
        pheromones=pheromones*(1-rho);
        pheromoneConstant=pheromoneConstant*(1-rho);
    end