function [pheromones]=newUpdatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray)
%updatePheromones This function updates the pheromone amounts within arcs
% in a given graph
% dimensionsElements is a cell array that contains for each dimension its
% associated decision elements
dimensionsNumber=length(dimensionsElements);

for antIndex=antsNumber
    antSolution=antsSolutions(antIndex,:);
    AllelementsIndexes=zeros(antsNumber,dimensionsNumber);
    for dimension=1:dimensionsNumber
            %% gather ant solution components indexes
            element=antSolution(dimension);
            AllelementsIndexes(antIndex,dimension)=find(dimensionsElements{dimension}==element);
    end
end

for dimensionInd=1:dimensionsNumber
    dimensionPheromones=pheromones{dimensionInd};
    for antInd=1:antsNumber
        antSolution=antsSolutions(antInd,:);
        elementsIndexes=AllelementsIndexes(antInd,:);
        
        %% calculate ant deposition based on fitness = cost^-1
        fitness=feval(fitnessFunction,antSolution,objectivesArray);
        antDepostion=Q*fitness;
        
        %% add the antDeposition to the pheromones amount contained in used arcs
        
        if dimensionInd==1
            firstNodeIndex=elementsIndexes(dimensionInd);
            arcPheromone=dimensionPheromones{firstNodeIndex};
            if isempty(arcPheromone)
                dimensionPheromones{firstNodeIndex}=pheromoneConstant+antDepostion;
            else
                dimensionPheromones{firstNodeIndex}=arcPheromone+antDepostion;
            end
        else
            actualElementIndex=elementsIndexes(dimensionInd);
            previousElementIndex=elementsIndexes(dimensionInd-1);
            arcPheromone=dimensionPheromones{previousElementIndex,actualElementIndex};
            if isempty(arcPheromone)
                dimensionPheromones{previousElementIndex,actualElementIndex}=pheromoneConstant+antDepostion;
            else
                dimensionPheromones{previousElementIndex,actualElementIndex}=arcPheromone+antDepostion;
            end
        end
    end
    pheromones{dimensionInd}=dimensionPheromones;
end
end