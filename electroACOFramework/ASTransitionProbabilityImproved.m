function probabilities=ASTransitionProbability(alpha,beta,dimensionsElements,fitnessArray,dimension,antSolution,pheromones,pheromoneConstant)
%% list pheromones associated with arcs upon to be evaluated on the current iteration
if dimension==1  
    dimensionElements=nonzeros(dimensionsElements(dimension,:));
    nodesNumber=length(dimensionElements);
%     pathsPheromone=pheromones{1};
    pathsPheromone=pheromones(1,1,:);
else
    lastDimensionElements=nonzeros(dimensionsElements(dimension-1 ,:));
    nodesNumber=length(nonzeros(dimensionsElements(dimension,:)));
    pathsPheromone=pheromones(dimension,lastDimensionElements==antSolution(dimension-1),:);
end

%check paths never used.
pathsPheromone(pathsPheromone==0)=pheromoneConstant;
pathsPheromone=full(pathsPheromone);
pathsPheromone=squeeze(pathsPheromone(1:nodesNumber));
probabilities=zeros(1,nodesNumber);
if isempty(find(fitnessArray, 1))
    %% probabilites based only in pheromone amounts for functions without cost or too high costs
    denom=sum(pathsPheromone.^alpha);
    for nodeIndex=1:nodesNumber
        numerator=pathsPheromone(nodeIndex)^alpha;
        probabilities(nodeIndex)=numerator/denom;
    end
else
    %% probabilites for functions that use costs
    denom=sum((fitnessArray.^beta).*(pathsPheromone.^alpha));
    %calculate transition probability for each node using ants
    %empirical transition function
    for nodeIndex=1:nodesNumber
        if fitnessArray(nodeIndex)~=0
            numerator=fitnessArray(nodeIndex)^beta*pathsPheromone(nodeIndex)^alpha;
            probabilities(nodeIndex)=numerator/denom;
        end
    end
    
end
end
