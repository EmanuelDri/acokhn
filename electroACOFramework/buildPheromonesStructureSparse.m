function pheromones=buildPheromonesStructureSparse(dimensionsSizes)
dimensionsNumber=length(dimensionsSizes);
        largestDimensionSize=max(dimensionsSizes);
        pheromones=sparse(dimensionsNumber*largestDimensionSize,largestDimensionSize);
%         for dimension=1:dimensionsNumber
%             switch dimension
%                 case  1
%                     pheromones{dimension}=sparse(1,dimensionsSizes(1));
%                 otherwise
%                     pheromones{dimension}=sparse(dimensionsSizes(dimension-1),dimensionsSizes(dimension));
%             end
%         end
end
