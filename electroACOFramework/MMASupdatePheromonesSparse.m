function [pheromones]=MMASupdatePheromonesSparse(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray,minimumPheromone,maximumPheromone)
%updatePheromones This function updates the pheromone amounts within arcs
% in a given graph
% dimensionsElements is a cell array that contains for each dimension its
% associated decision elements
[dimensionsNumber,~]=size(dimensionsElements);
dimensionsSizes=zeros(1,dimensionsNumber);
for dimensionIndex=1:dimensionsNumber
    dimensionsSizes(dimensionIndex)=length(nonzeros(dimensionsElements(dimensionIndex,:)));
end
largestDimensionSize=max(dimensionsSizes);
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    elementsIndexes=zeros(1,dimensionsNumber);
    for dimension=1:dimensionsNumber
        %% gather ant solution components indexes
        element=antSolution(dimension);
        dimensionElements=nonzeros(dimensionsElements(dimension,:));
        elementsIndexes(dimension)=find(dimensionElements==element);
    end
    %% calculate ant deposition based on fitness = cost^-1
    fitness=feval(fitnessFunction,antSolution,objectivesArray);
    antDepostion=Q*fitness;
    %%
    for dimensionInd=1:dimensionsNumber
        %% add the antDeposition to the pheromones amount contained in used arcs
%         dimensionPheromones=pheromones(dimensionInd,:,:);
        if dimensionInd==1
            firstNodeIndex=elementsIndexes(dimensionInd);
            arcPheromone=nonzeros(pheromones(firstNodeIndex,1));
            if isempty(arcPheromone)
                newArcPheromone=pheromoneConstant+antDepostion;
            else
                
                if arcPheromone<minimumPheromone
                    %% check if the pheromone contained in the arc is less than the minimum
                    arcPheromone=minimumPheromone;
                end
                
                newArcPheromone=arcPheromone+antDepostion;
            end
            
            if newArcPheromone>maximumPheromone
                %% check if pheromone contained in the arc is more than the maximum
                newArcPheromone=maximumPheromone;
            end
            pheromones(firstNodeIndex,1)=newArcPheromone;
        else
            actualElementIndex=elementsIndexes(dimensionInd);
            previousElementIndex=elementsIndexes(dimensionInd-1);
            arcPheromone=pheromones((dimensionInd-1)*largestDimensionSize+previousElementIndex,actualElementIndex);
            arcPheromone=nonzeros(arcPheromone);
            if isempty(arcPheromone)
                newArcPheromone=pheromoneConstant+antDepostion;
            else
                
                if arcPheromone<minimumPheromone
                    %% check if pheromone contained in the arc is more than the maximum
                    arcPheromone=minimumPheromone;
                end
                
                newArcPheromone=arcPheromone+antDepostion;
                if newArcPheromone>maximumPheromone
                    %% check if pheromone contained in the arc is more than the maximum
                    newArcPheromone=maximumPheromone;
                end
            end
            pheromones((dimensionInd-1)*largestDimensionSize+previousElementIndex,actualElementIndex)=newArcPheromone;
        end
%         pheromones{dimensionInd}=dimensionPheromones;
    end
end
end