function [gError,wpError,qpError,maxError,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray)
%% get values from input arrays
r1=valuesArray(1);
r2=valuesArray(2);
r3=valuesArray(3);
r4=valuesArray(4);
r5=valuesArray(5);
r6=valuesArray(6);
c1=valuesArray(7);
c2=valuesArray(8);
objectiveG=objectivesArray(1);
objectiveWp=objectivesArray(2);
objectiveQp=objectivesArray(3);
maxError=objectivesArray(4);
%% calculate g,wp and qp
G=gain(r3,r4,r5,r6);
wp=poleFrequency(r1,r2,r5,r6,c1,c2);
qp=qualityFactor(r1,r2,r3,r4,r5,r6,c1,c2);
%% calculate errors
gError=relativeError(G,objectiveG)*100;
wpError=relativeError(wp,objectiveWp)*100;
qpError=relativeError(qp,objectiveQp)*100;
%% calculate sensitivities
sens1=sensitivityFunction1(r3,r4);
sens2=sensitivityFunction2(r1,r2,r3,r4,r5,r6,c1,c2);
end