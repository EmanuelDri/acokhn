function fitness=filterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
fitness=sum([gError wpError qpError])+100*(sens1+sens2);
fitness=1/fitness;
end