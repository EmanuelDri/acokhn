hold off
figure(2)
numTF=[0 0 3*(2000*pi)^2 ];
denomTF=[1 2000*pi/0.707 (2000*pi)^2];
w=0:1000:10e4;
%Function �freqs� gives the frequency response in the s-domain
Y=freqs(numTF,denomTF,w);
y1=abs(Y);
y2=angle(Y);
% subplot(2,1,1)
enHz=w/2/pi;
semilogx(enHz,20*log10(y1),'--')
hold on
r1=2940;r2=66500;r3=6040;r4=5490;r5=499000;r6=97600;c1=0.000000024;
c2=0.0000000011;
g=gain(r3,r4,r5,r6)
wp=poleFrequency(r1,r2,r5,r6,c1,c2)
qp=qualityFactor(r1,r2,r3,r4,r5,r6,c1,c2)
numerador=[0 0 g*wp^2]
denominador=[1 wp/qp wp^2]
amplitud=freqs(numerador,denominador,w);
amplitud=abs(amplitud);
semilogx(enHz,20*log10(amplitud),'.-r')
grid on
ylabel('Amplitud (dB)')
title('Respuesta en frecuencia')
% subplot(2,1,2)
% semilogx(w,y2*(180/pi))
% grid on
% ylabel('Phase (deg))')
xlabel('Frequencia (Hz)')
hold off