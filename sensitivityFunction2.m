function sensitivity = sensitivityFunction2( r1,r2,r3,r4,r5,r6,c1,c2 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
qp=qualityFactor(r1,r2,r3,r4,r5,r6,c1,c2);
auxTerm1=-qp/2;
auxTerm2=(r5-r6)/(1+r4/r3);
auxTerm3=sqrt(r2*c2/r5/r6/r1/c1);
sensitivity=abs(auxTerm1*auxTerm2*auxTerm3);

end

