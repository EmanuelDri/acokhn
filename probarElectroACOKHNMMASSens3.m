estadisticasContainer=containers.Map;
tic
win=randperm(100);
% win=win(1);
win=1004;


Q=3;
for numeroDeHormigas=[150 ];
numeroDeIteraciones=1700%1300;
for mutationRate=0%[1.1];
for initialPheromone=0.1%[ .007 ];
for pheromoneFactor=32.5%[1.9];
for rho=.27%[ .38 ]
metodoDeSeleccion='ordered roulette' %'uniform';
escenario=2;
for alpha=1.23%[.945]
for funcionDeCosto=[2 10] 
for ejecucion=1:10
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR12(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,Q,alpha,0,5,metodoDeSeleccion,escenario,funcionDeCosto,win,pheromoneFactor,mutationRate)
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor funcionDeCosto mutationRate win]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN2_sensibilidad_R12'  '23 de julio side b'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc

end
end
end
end
end
end
end
end

win
toc
format shortg
c = clock
