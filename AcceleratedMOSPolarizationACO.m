function [nodesList,totalCost,idf,vdsf]=AcceleratedMOSPolarizationACO(rho,antsNumber,iterationsNumber,Vcc,Vto,K,Idq,Vdsq,initialPheromoneConstant,Q,alpha,beta,errorPercentage,modoSeleccion)
%
%% startup
pheromoneConstant=initialPheromoneConstant;
dimensionsNumber=3;
antsSolutions=zeros(antsNumber,dimensionsNumber);
idealVgs=sqrt(Idq/K)+Vto;
possibleVgValues=zeros(1,91*91);
resistorValues=resistorCommercialValues;
% Define possible Vg values. Those Vg values that generate a Vgs lower fitnessesthan
% Vto are discarded

rsMin=min(resistorValues);
rsMax=max(resistorValues);
for r1Index=1:91
    for r2Index=1:91
        r1=resistorValues(r1Index);
        r2=resistorValues(r2Index);
        %         resistanceCombinationError=abs((Vcc-Vdsq)/(r1+r2)-Idq)/Idq;
        vg=Vcc*r2/(r1+r2);
        vgsMin=vg-Idq*(1-errorPercentage)*rsMin;
        vgsMax=vg-Idq*(1+errorPercentage)*rsMax;
        if (vgsMin>=Vto)||(vgsMax>=Vto)
            possibleVgValues(r1Index+r2Index-1)=vg;
        end
    end
end
possibleVgValues=truncateInto(possibleVgValues,1);
possibleVgValues=unique(possibleVgValues);
possibleVgValues(possibleVgValues==0)=[];
pheromones=buildPheromonesStructure;

%Generate an initial solution in order to improve it later during the
%program iterations
for antIndex=1:antsNumber
    % Initialize ants solutions setting two random resistances
    for dimensionIndex=1:dimensionsNumber-1
        antsSolutions(antIndex,dimensionIndex)=randomResistor;
    end
    idealVg=idealVgs+Idq*antsSolutions(antIndex,2);
    [~,vgIndex]=min(abs(possibleVgValues-ones(1,length(possibleVgValues))*idealVg));
    antsSolutions(antIndex,dimensionsNumber)=possibleVgValues(vgIndex);
end

%% iterate
for iteration=1:iterationsNumber
    for antIndex=1:antsNumber
        for dimensionIndex=1:dimensionsNumber
            [~,nodes]=size(pheromones{dimensionIndex});
            fitnesses=zeros(1,length(nodes));
            switch dimensionIndex
                case 1
                    rs=antsSolutions(antIndex,2);
                    vg=antsSolutions(antIndex,3);
                    for resistorIndex=1:length(resistorValues)
                        rd=resistorValues(resistorIndex);
                        fitnesses(resistorIndex)=MOSOutputResistanceFitness(rd,rs,vg);
                    end
                case 2
                    rd=antsSolutions(antIndex,1);
                    vg=antsSolutions(antIndex,3);
                    for resistorIndex=1:length(resistorValues)
                        rs=resistorValues(resistorIndex);
                        fitnesses(resistorIndex)=MOSOutputResistanceFitness(rd,rs,vg);
                    end
                case 3
                    rd=antsSolutions(antIndex,1);
                    rs=antsSolutions(antIndex,2);
                    for vgIndex=1:length(possibleVgValues)
                        vg=possibleVgValues(vgIndex);
                        fitnesses(vgIndex)=MOSOutputResistanceFitness(rd,rs,vg);
                    end
            end
            antSolution=antsSolutions(antIndex,:);
            transitionProbabilities=transitionProbability(fitnesses,dimensionIndex,antSolution);
            nextNodeIndex=seleccionarProximoNodo(transitionProbabilities,modoSeleccion);
%             nextNodeIndex=ruleta('desordenada',transitionProbabilities);
            if dimensionIndex==dimensionsNumber
                antsSolutions(antIndex,dimensionIndex)=possibleVgValues(nextNodeIndex);
            else
                antsSolutions(antIndex,dimensionIndex)=resistorValues(nextNodeIndex);
            end
            
        end
        acceleratedPheromonesUpdate(antIndex,dimensionIndex);
    end
%     updatePheromones;
    evaporatePheromones;
    
end
[nodesList,totalCost,idf,vdsf]=selectBestPath;

%% auxiliar functions
    function [path,cost,idf,vdsf]=selectBestPath
        cost=Inf;
        for antInd=1:antsNumber
            antSolution=antsSolutions(antInd,:);
            rd=antSolution(1);
            rs=antSolution(2);
            vg=antSolution(3);
            adequacy=MOSOutputResistanceFitness(rd,rs,vg);
            if adequacy<cost
                cost=adequacy;
                path=[rd rs vg];
            end
            
        end
        rd=path(1);
        rs=path(2);
        vg=path(3);
        vgs=getVgs(rs,vg);
        idf=(vg-vgs)/rs;
        vdsf=Vcc-idf*(rs+rd);        
    end

    function r=randomResistor
        valuesNumber=length(resistorCommercialValues);
        randomValue=1+floor(rand*valuesNumber);
        r=resistorValues(randomValue);
    end

function indiceProximoNodo=seleccionarProximoNodo(probabilidades,modoSeleccion)
        switch modoSeleccion
            case 'ruleta'
                indiceProximoNodo=ruleta('desordenada',probabilidades);
            case 'uniforme'
                [~,indiceProximoNodo]=max(probabilidades.*rand(1,length(probabilidades)));
            case 'torneo'
                indiceProximoNodo=torneo(probabilidades,paramTorneo);
            case 'ruleta ordenada'
                indiceProximoNodo=ruleta('ordenada',probabilidades);
            case 'ruleta alreves'
                indiceProximoNodo=ruleta('ordenada alreves',probabilidades);
        end
    end

    function indiceProximoNodo=ruleta(modo,probabilidades)
        switch modo
            case 'desordenada'
                probabilidadesOrdenadas=probabilidades;
                indicesDelOrdenamiento=1:length(probabilidades);
            case 'ordenada'
                [probabilidadesOrdenadas,indicesDelOrdenamiento]=ordenamientoZA(probabilidades);
            case 'ordenada alreves'
                [probabilidadesOrdenadas,indicesDelOrdenamiento]=ordenamientoAZ(probabilidades);
        end
        cursorRuleta=rand;
        indiceProximoNodo=0;
        indiceRuleta=1;
        while indiceProximoNodo==0
            if cursorRuleta<sum(probabilidadesOrdenadas(1:indiceRuleta))
                indiceProximoNodo=indiceRuleta;
            end % if
            indiceRuleta=indiceRuleta+1;
        end % iz
        indiceProximoNodo=indicesDelOrdenamiento(indiceProximoNodo);
    end

    function f=MOSOutputResistanceFitness(rd,rs,vg)
        Vgs=getVgs(rs,vg);
        if ((Vgs<Vto))
            f=inf;
        else
            id=K*(Vgs-Vto)^2;
            vds=Vcc-id*(rs+rd);
            if vds>0
                vdsError=abs(Vdsq-vds)/Vdsq*100;
                idError=abs(Idq-id)/Idq*100;
                f=norm([vdsError 2*idError]);
            end
        end
    end

    function Vgs=getVgs(rs,vg)
        a=        rs*K;
        b=-2*Vto*K*rs+1;
        c=K*rs*Vto^2-vg;
        [Vgs1,Vgs2]=quadraticSolver(a,b,c);
        Vgs=max([Vgs1 Vgs2]);
    end


    function [x1,x2]=quadraticSolver(a,b,c)
        aux1=-b/2/a;
        aux2=sqrt(b^2-4*a*c)/2/a;
        x1=aux1-aux2;
        x2=aux1+aux2;
    end

    function distance=euclideanDistance(v1,v2)
        distance=sqrt((v1-v2).^2*ones(length(v1),1));
    end

    function pheromones=buildPheromonesStructure
        pheromones=cell(1,dimensionsNumber);
        for dimension=1:dimensionsNumber
            switch dimension
                case  1
                    pheromones{dimension}=cell(1,length(resistorValues));
                case dimensionsNumber
                    pheromones{dimension}=cell(length(resistorValues),length(possibleVgValues));
                otherwise
                    pheromones{dimension}=cell(length(resistorValues),length(resistorValues));
            end
        end
    end

    function probabilities=transitionProbability(fitnessArray,dimension,antSolution)
        %list pheromones associated with arcs upon to ebe evaluated on
        %the current iteration
        
        switch dimension
            case  1
                nodesNumber=length(resistorValues);
                pathsPheromone=pheromones{1};
            case dimensionsNumber
                nodesNumber=length(possibleVgValues);
                pathsPheromone=pheromones{dimension};
                pathsPheromone=pathsPheromone(find(resistorValues==antSolution(dimension-1)),:);
            otherwise
                nodesNumber=length(resistorValues);
                pathsPheromone=pheromones{dimension};
                pathsPheromone=pathsPheromone(find(resistorValues==antSolution(dimension-1)),:);
        end
        
        %check paths never used. 
        pathsPheromone= cellfun(@(x) cellCheck(x),pathsPheromone,'UniformOutput',true);
        
        probabilities=zeros(1,nodesNumber);
    
        denom=sum((1./fitnessArray.^beta).*(pathsPheromone.^alpha));
        
        %calculate transition probability for each node using ants
        %empirical transition function
        for nodeIndex=1:nodesNumber
            numerator=1/fitnessArray(nodeIndex)^beta*pathsPheromone(nodeIndex)^alpha;
            probabilities(nodeIndex)=numerator/denom;
        end
        
        function status=cellCheck(element)
            %This function returns the iteration pheromone constant in case
            %the evaluated phermone cell is empty otherwise it returns its
            %content
            if isempty(element)
                status=pheromoneConstant;
            else
                status=element;
            end
        end
        
    end

    function updatePheromones
        for dimensionInd=1:dimensionsNumber
            dimensionPheromones=pheromones{dimensionInd};
            for antInd=1:antsNumber
                antSolution=antsSolutions(antInd,:);
                rd=antSolution(1);
                rs=antSolution(2);
                vg=antSolution(3);
                aptness=MOSOutputResistanceFitness(rd,rs,vg);
                antDepostion=Q/aptness;
                firstNodeIndex=find(resistorValues==rd);
                secondNodeIndex=find(resistorValues==rs);
                thirdNodeIndex=find(possibleVgValues==vg);
                switch dimensionInd
                    case 1
                        arcPheromone=dimensionPheromones{firstNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{firstNodeIndex}=pheromoneConstant+antDepostion;
                        else
                            dimensionPheromones{firstNodeIndex}=dimensionPheromones{firstNodeIndex}+antDepostion;
                        end
                    case 2
                        arcPheromone=dimensionPheromones{firstNodeIndex,secondNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{firstNodeIndex,secondNodeIndex}=pheromoneConstant+antDepostion;
                        else
                            dimensionPheromones{firstNodeIndex,secondNodeIndex}=dimensionPheromones{firstNodeIndex,secondNodeIndex}+antDepostion;
                        end
                    case 3
                        arcPheromone=dimensionPheromones{secondNodeIndex,thirdNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{secondNodeIndex,thirdNodeIndex}=    pheromoneConstant+antDepostion;
                        else
                            dimensionPheromones{secondNodeIndex,thirdNodeIndex}=dimensionPheromones{secondNodeIndex,thirdNodeIndex}+antDepostion;
                        end
                end
                pheromones{dimensionInd}=dimensionPheromones;
            end
        end
    end
function acceleratedPheromonesUpdate(antInd,dimensionInd)
    antSolution=antsSolutions(antInd,:);
                rd=antSolution(1);
                rs=antSolution(2);
                vg=antSolution(3);
                aptness=MOSOutputResistanceFitness(rd,rs,vg);
                antDepostion=Q/aptness;
                firstNodeIndex=find(resistorValues==rd);
                secondNodeIndex=find(resistorValues==rs);
                thirdNodeIndex=find(possibleVgValues==vg);
%         for dimensionInd=1:dimensionsNumber
            dimensionPheromones=pheromones{dimensionInd};
%             for antInd=1:antsNumber
                switch dimensionInd
                    case 1
                        arcPheromone=dimensionPheromones{firstNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{firstNodeIndex}=pheromoneConstant*(1-rho)+antDepostion;
                        else
                            dimensionPheromones{firstNodeIndex}=dimensionPheromones{firstNodeIndex}*(1-rho)+antDepostion;
                        end
                    case 2
                        arcPheromone=dimensionPheromones{firstNodeIndex,secondNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{firstNodeIndex,secondNodeIndex}=pheromoneConstant*(1-rho)+antDepostion;
                        else
                            dimensionPheromones{firstNodeIndex,secondNodeIndex}=dimensionPheromones{firstNodeIndex,secondNodeIndex}*(1-rho)+antDepostion;
                        end
                    case 3
                        arcPheromone=dimensionPheromones{secondNodeIndex,thirdNodeIndex};
                        if isempty(arcPheromone)
                            dimensionPheromones{secondNodeIndex,thirdNodeIndex}=    pheromoneConstant*(1-rho)+antDepostion;
                        else
                            dimensionPheromones{secondNodeIndex,thirdNodeIndex}=dimensionPheromones{secondNodeIndex,thirdNodeIndex}*(1-rho)+antDepostion;
                        end
                end
                pheromones{dimensionInd}=dimensionPheromones;
%             end
%         end
    end
    function evaporatePheromones
        for dimensionInd=1:dimensionsNumber
            dimensionCells=pheromones{dimensionInd};
            dimensionCells=cellfun(@(x) x*(1-rho),dimensionCells,'UniformOutput',false);
            pheromones{dimensionInd}=dimensionCells;
            pheromoneConstant=pheromoneConstant*(1-rho);
        end
    end

    function truncatedValues=truncateInto(inputValues,decimalsNumber)
        truncatedValues=floor(10^decimalsNumber*inputValues)/10^decimalsNumber;
    end
end