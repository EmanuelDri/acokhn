estadisticasContainer=containers.Map;
tic
win=randperm(100);
% win=win(1);
win=1003;


Q=3;
for numeroDeHormigas=[150 ];
numeroDeIteraciones=1300;
for mutationRate=[1.1 ];
initialPheromone=[.05 ];
for pheromoneFactor=[1.9];
for rho=[ .37 ];
metodoDeSeleccion='uniform';
escenario=2;
for alpha=[.925 .927 .93 .935 .94 .945 .95 .97 .99 .995 1 1.05 1.1]
for funcionDeCosto=[2] 
for ejecucion=1:15
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR12(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,Q,alpha,0,5,metodoDeSeleccion,escenario,funcionDeCosto,win,pheromoneFactor,mutationRate)
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor funcionDeCosto mutationRate win]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN2_sensibilidad_R12'  '_67s.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc

end
end
% end
end
end
end
end
end

win
toc
format shortg
c = clock
