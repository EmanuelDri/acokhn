function fitness=filterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
% fitness=sum([gError wpError qpError])+100*sum([sens1 sens2])+ exp(max([gError wpError qpError])/4.5-1)+exp(100*max([sens1 sens2])/18.9-1);
% fitness=1/fitness;
% distance=abs(max([gError wpError qpError])-min([gError wpError qpError]));
% fitness=1/((norm([gError wpError qpError])+norm([ 100*sens1 100*sens2]))+distance);
%  fitness=sum([gError wpError qpError].^1.225)+100*sum([sens1^.98 sens2^1.5]);
%  fitness=sum([gError wpError qpError].^1.225)+100*sum([sens1^.98 sens2^1.7]);
% fitness=sum([gError wpError qpError].^1.3)+100*sum([sens1^.98 sens2^1.7]);
% fitness=sum([gError wpError qpError].^1.3)+100*sum([sens1^1.1 sens2^1.7]);
% fitness=sum([gError wpError qpError].^1.3)+100*sum([sens1^1.1 sens2^1.7]);
% fitness=sum([gError wpError qpError].^1.4)+100*sum([sens1^1.3 sens2^.1]);
% fitness=sum([gError wpError qpError].^1.8)+100*sum([sens1^3 sens2^.1]);
% fitness=sum([gError wpError
% qpError].^1.859)+(100*sens1)^1.8+(100*sens2)^.8; quiero bajar las it,
% este parece andar bien
% fitness=sum([gError wpError qpError].^1.9)+(100*sens1)^1.8+(100*sens2)^.8;
% fitness=sum([gError wpError qpError].^2)+(100*sens1)^1.8+(100*sens2)^.8;
% fitness=sum([gError wpError qpError].^2)+(100*sens1)^1.8+(100*sens2)^.8;
maximo=max([gError wpError qpError]);
if maximo<=5
    maximo=0;
end
% fitness=sum([gError wpError qpError].^2.2)+(100*sens1)^1.7+(5*sens2)-maximo^2.2+maximo^2.5;
% fitness=sum([gError wpError
% qpError].^2.2)+(100*sens1)^2.7+(5*sens2)-maximo^2.2+maximo^2.5; parece
% funcionar, la sens1 en 3 corridas son menores a 0.2, en una corrida
% posterior, se hallo un caso para el cual uno de los errores fue mayor al
% 20%
fitness=sum([gError wpError qpError].^2.2)+(100*sens1)^2.7+(15*sens2)-maximo^2.2+maximo^2.52;
 fitness=1/fitness;
 if 1.5*sum([gError wpError qpError]>5)==0&&sum([sens1 sens2]>0.5)==0
     fitness=fitness*40;
 end
end