function fitness=filterFitness9(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
if sum([gError wpError qpError]>5)>0%||sum([sens1 sens2]>.5)>0
% if gError<5
%     gError=5;
% end
% if qpError<5
%     qpError=5;
% end
% if wpError<5
%     wpError=5;
% end
if sens1<.5
    sens1=.5;
end
if sens2<.5
    sens2=.5;
end

end
fitness=sum([gError wpError qpError])+100*(sens1+sens2);
fitness=1/fitness;
end

