estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=450
    for numeroDeHormigas=400
        for rho=.1
            %             for alpha=1:5
            %                 for beta=1:5
            for pheromoneFactor=[4 8 16 24 32 64]
                alpha=1;
                beta=0;
                for Q=3
                    for iteration=1:4
                        
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOFullMMAS(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),7,3,alpha,beta,inf,'ordered roulette',2,3,8,pheromoneFactor);
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones pheromoneFactor]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_prueba_Workin_FullMMAS'  '.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc
                        
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
