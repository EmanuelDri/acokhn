function [orderedVector,indexesBeforeOrdering] = order( vector,mode)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

switch mode
    case 'minorToMajor'
        operation='min';
        sustitute=inf;
    case 'majorToMinor'
        operation='max';
        sustitute=0;
end

orderedVector=zeros(1,length(vector));
indexesBeforeOrdering=orderedVector;
for index=1:length(vector)
    [element,pos]=feval(operation,vector);
    orderedVector(index)=element;
    vector(pos)=sustitute;
    indexesBeforeOrdering(index)=pos;
end