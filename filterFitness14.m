function fitness=filterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
fitness=.10*sum([gError wpError qpError])+0100*(sens1+sens2)+010*max([gError wpError qpError])+00*max([sens1 sens2]);
fitness=1/fitness;
end