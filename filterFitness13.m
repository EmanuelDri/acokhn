function fitness=filterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
fitness=.1*sum([gError wpError qpError])+10*(sens1+sens2)+1*max([gError wpError qpError])+100*max([sens1 sens2]);
fitness=1/fitness;
end