function [bestPath,bestCost,gError,wpError,qpError,sens1,sens2]=randomIniPheromoneKHNACO(dimensionsNumber,rho,antsNumber,iterationsNumber,g,wp,qp,initialPheromoneConstant,Q,alpha,beta,errorPercentage,modoSeleccion,componentsMode,fitnessMode)
%
%% import auxiliar functions
tic
addpath 'electroACOFramework\';
%% startup
pheromoneConstant=initialPheromoneConstant;
antsSolutions=zeros(antsNumber,dimensionsNumber);
dimensionsElements=cell(1,dimensionsNumber);

switch componentsMode
    case 1
        resistorsSerie='E96';
        capacitorsSerie='E24';
    case 2
        resistorsSerie='E24';
        capacitorsSerie='E12';
end

resistors=unique(resistorCommercialValues(resistorsSerie));
capacitors=unique(capacitorCommercialValues(capacitorsSerie));
capacitorsNumber=length(capacitors);
resistorsNumber=length(resistors);

switch fitnessMode
    case 1
        fitnessFunction='euclideanFilterFitness';
    case 2
        fitnessFunction='filterFitness';
    case 3
        fitnessFunction='edoSenseiNoFilterFitness';
    case 4
        fitnessFunction='ameNoFilterFitness';
end

objectivesArray=[g wp qp errorPercentage];

%% initial random solution and dimensions setup

dimensionsSizes=zeros(1,dimensionsNumber);

for dimensionIndex=1:6
    dimensionsElements{dimensionIndex}=resistors;
    dimensionsSizes(dimensionIndex)=resistorsNumber;
    for ant=1:antsNumber
        antsSolutions(ant,dimensionIndex)=randomElement(resistors);
    end
end
for dimensionIndex=7:8
    dimensionsElements{dimensionIndex}=capacitors;
    dimensionsSizes(dimensionIndex)=capacitorsNumber;
    for ant=1:antsNumber
        antsSolutions(ant,dimensionIndex)=randomElement(capacitors);
    end
end
[bestPath,bestCost,bestAnt]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
% bestCost=inf;
pheromones=buildPheromonesStructure(dimensionsSizes);
pheromones=updatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray);
pheromones=evaporatePheromones(pheromones,pheromoneConstant,rho);

%% iterate
toc
tic
for iteration=1:iterationsNumber
    for antIndex=1:antsNumber
%         newAntSolution=zeros(1,dimensionsNumber);
%         %         feasibleSolution=false;
%         %         while ~feasibleSolution
            antSolution=antsSolutions(antIndex,:);
        for dimensionIndex=1:dimensionsNumber
%             dimensionSize=dimensionsSizes(dimensionIndex);
            dimensionElements=dimensionsElements{dimensionIndex};
%             nodes=dimensionsSizes(dimensionIndex);
%             fitnesses=zeros(1,length(nodes));

%             for elementIndex=1:dimensionSize
%                 element=dimensionElements(elementIndex);
%                 antSolution(dimensionIndex)=element;
%                 fitnesses(elementIndex)=feval(fitnessFunction,antSolution,objectivesArray);
%             end
            transitionProbabilities=ASTransitionProbability(alpha,beta,dimensionsElements,0,dimensionIndex,antSolution,pheromones,pheromoneConstant);
            nextNodeIndex=selectionMethod(modoSeleccion,transitionProbabilities);
            antSolution(dimensionIndex)=dimensionElements(nextNodeIndex);
            %             newAntSolution(dimensionIndex)=dimensionElements(nextNodeIndex);
%             pheromones=acceleratedPheromonesUpdate(dimensionIndex,dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antIndex,objectivesArray,rho);
        end
        %             [gError,wpError,qpError,~,~,~]=fitnessBaseOperations(newAntSolution,objectivesArray);
        %             if ~(gError>errorPercentage||wpError>errorPercentage||qpError>errorPercentage)
        %                 feasibleSolution=true;
        %             end
        antsSolutions(antIndex,:)=antSolution;
        %         antsSolutions(antIndex,:)=newAntSolution;
        
    end
    pheromones=updatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray);
    [pheromones,pheromoneConstant]=evaporatePheromones(pheromones,pheromoneConstant,rho);
    [path,cost,ant]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber);
    if cost<=bestCost
        bestPath=path;
        bestCost=cost;
        bestAnt=ant;
    end
    for dimension=1:dimensionsNumber
        pheromones=acceleratedPheromonesUpdate(dimension,dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,bestAnt,objectivesArray,rho);
    end
end
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(bestPath,objectivesArray);
toc
end

