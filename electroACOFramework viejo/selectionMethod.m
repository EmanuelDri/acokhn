function selectedIndex= selectionMethod( mode,probabilities)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
switch mode
    case 'roulette'
        selectedIndex=roulette('not ordered',probabilities);
    case 'uniform'
        [~,selectedIndex]=max(probabilities.*rand(1,length(probabilities)));
    case 'tournament'
        selectedIndex=torneo(probabilities,paramTorneo);
    case 'ordered roulette'
        selectedIndex=roulette('ordered',probabilities);
    case 'inverse ordered roulette'
        selectedIndex=roulette('inverse ordered',probabilities);
end
    function selectedIndex=roulette(modo,probabilities)
        switch modo
            case 'not ordered'
                orderedProbabilities=probabilities;
                orderingIndexes=1:length(probabilities);
            case 'ordered'
                [orderedProbabilities,orderingIndexes]=order(probabilities,'majorToMinor');
            case 'inverse ordered'
                [orderedProbabilities,orderingIndexes]=order(probabilities,'minorToMajor');
        end
        rouletteCursor=rand;
        selectedIndex=0;
        rouletteIndex=1;
        while selectedIndex==0
            if rouletteCursor<sum(orderedProbabilities(1:rouletteIndex))
                selectedIndex=rouletteIndex;
            end % if
            rouletteIndex=rouletteIndex+1;
        end % iz
        selectedIndex=orderingIndexes(selectedIndex);
    end
end

