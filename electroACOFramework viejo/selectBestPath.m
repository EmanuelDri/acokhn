function [route,cost,ant]=selectBestPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber)
cost=0;
ant=0;
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    adequacy=feval(fitnessFunction,antSolution,objectivesArray);
%     adequacy=1/adequacy;       
    if adequacy>=cost
        cost=adequacy;
        route=antSolution;
		ant=antInd;
    end
    
end
cost=1/cost;
if cost==inf
    [route,cost,ant]=selectBestPath(antsSolutions,'ameNoFilterFitness',objectivesArray,antsNumber);
end
        
end