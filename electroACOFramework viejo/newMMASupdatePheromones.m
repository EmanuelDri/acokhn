function [pheromones]=newMMASupdatePheromones(dimensionsElements,pheromones,antsSolutions,fitnessFunction,Q,pheromoneConstant,antsNumber,objectivesArray,minimumPheromone,maximumPheromone)
%updatePheromones This function updates the pheromone amounts within arcs
% in a given graph
% dimensionsElements is a cell array that contains for each dimension its
% associated decision elements
dimensionsNumber=length(dimensionsElements);
solutionsElementsIndexes=zeros(antsNumber,dimensionsNumber);
for dimension=1:dimensionsNumber
    
    dimensionElements=dimensionsElements{dimension};
    for ant=1:antsNumber
        %% gather ant solution components indexes
        element=antsSolutions(ant,dimension);
        solutionsElementsIndexes(ant,dimension)=find(dimensionElements==element);
    end
end

for dimensionInd=1:dimensionsNumber
    dimensionPheromones=pheromones{dimensionInd};
    for antInd=1:antsNumber
        antSolution=antsSolutions(ant,:);
        elementsIndexes=solutionsElementsIndexes(ant,:);
        %% calculate ant deposition based on fitness = cost^-1
        fitness=feval(fitnessFunction,antSolution,objectivesArray);
        antDepostion=Q*fitness;
        %%
        
        %% add the antDeposition to the pheromones amount contained in used arcs
        if dimensionInd==1
            firstNodeIndex=elementsIndexes(dimensionInd);
            arcPheromone=dimensionPheromones{firstNodeIndex};
            if isempty(arcPheromone)
                dimensionPheromones{firstNodeIndex}=pheromoneConstant+antDepostion;
            else
                
                if arcPheromone<minimumPheromone
                    %% check if the pheromone contained in the arc is less than the minimum
                    arcPheromone=minimumPheromone;
                end
                
                dimensionPheromones{firstNodeIndex}=arcPheromone+antDepostion;
            end
            
            if dimensionPheromones{firstNodeIndex}>maximumPheromone
                %% check if pheromone contained in the arc is more than the maximum
                dimensionPheromones{firstNodeIndex}=maximumPheromone;
            end
        else
            actualElementIndex=elementsIndexes(dimensionInd);
            previousElementIndex=elementsIndexes(dimensionInd-1);
            arcPheromone=dimensionPheromones{previousElementIndex,actualElementIndex};
            if isempty(arcPheromone)
                dimensionPheromones{previousElementIndex,actualElementIndex}=pheromoneConstant+antDepostion;
            else
                
                if arcPheromone<minimumPheromone
                    %% check if pheromone contained in the arc is more than the maximum
                    arcPheromone=minimumPheromone;
                end
                
                dimensionPheromones{previousElementIndex,actualElementIndex}=arcPheromone+antDepostion;
                if dimensionPheromones{previousElementIndex,actualElementIndex}>maximumPheromone
                    %% check if pheromone contained in the arc is more than the maximum
                    dimensionPheromones{previousElementIndex,actualElementIndex}=maximumPheromone;
                end
            end
        end
        
    end
    pheromones{dimensionInd}=dimensionPheromones;
end
end