function probabilities=ASTransitionProbability(alpha,beta,dimensionsElements,fitnessArray,dimension,antSolution,pheromones,pheromoneConstant)
%% list pheromones associated with arcs upon to be evaluated on the current iteration
if dimension==1
    dimensionElements=dimensionsElements{dimension};
    nodesNumber=length(dimensionElements);
    pathsPheromone=pheromones{1};
else
    lastDimensionElements=dimensionsElements{dimension-1};
    nodesNumber=length(dimensionsElements{dimension});
    pathsPheromone=pheromones{dimension};
    pathsPheromone=pathsPheromone(lastDimensionElements==antSolution(dimension-1),:);
end

%check paths never used.
pathsPheromone= cellfun(@(x) cellCheck(x),pathsPheromone,'UniformOutput',true);
probabilities=zeros(1,nodesNumber);
if isempty(find(fitnessArray, 1))
    %% probabilites based only in pheromone amounts for functions without cost or too high costs
    denom=sum(pathsPheromone.^alpha);
    for nodeIndex=1:nodesNumber
        numerator=pathsPheromone(nodeIndex)^alpha;
        probabilities(nodeIndex)=numerator/denom;
    end
else
    %% probabilites for functions that use costs
    denom=sum((fitnessArray.^beta).*(pathsPheromone.^alpha));
    %calculate transition probability for each node using ants
    %empirical transition function
    for nodeIndex=1:nodesNumber
        if fitnessArray(nodeIndex)~=0
            numerator=fitnessArray(nodeIndex)^beta*pathsPheromone(nodeIndex)^alpha;
            probabilities(nodeIndex)=numerator/denom;
        end
    end
    
end

    function status=cellCheck(element)
        %This function returns the iteration pheromone constant in case
        %the evaluated phermone cell is empty otherwise it returns its
        %content
        if isempty(element)
            status=pheromoneConstant;
        else
            status=element;
        end
    end

end
