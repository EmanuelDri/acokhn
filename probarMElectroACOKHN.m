estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=30
    for numeroDeHormigas=30
        for rho=.9
            %             for alpha=1:5
            %                 for beta=1:5
            for confVector=[[2 2];[1 3];[1 2]]
                alpha=confVector(1);
                beta=confVector(2);
                for Q=3
                    for errorPercentage=60
                        for ejecucion=1:10
                            tic
                            ejecucion
                            [path,cost,gError,wpError,qError,sens1,sens2]=MAcceleratedKHNACO(8,.9,30,30,3,2000*pi,1/sqrt(2),.1,3,alpha,beta,60,'uniform',2,3);
                            linea=num2str([path,cost,gError,wpError,qError,sens1,sens2 Q rho alpha beta numeroDeIteraciones numeroDeHormigas errorPercentage]);
                            linea=[linea '\n'];
                            archivo=fopen(['KHN_M_prueba_1'  'ma.txt'],'a');
                            fprintf(archivo,linea);
                            fclose(archivo);
                            
                            toc
                        end
                    end
                end
                %                 end
            end
        end
    end
end

toc
format shortg
c = clock
