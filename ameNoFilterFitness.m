function fitness=ameNoFilterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
% if gError>maxError||qpError>maxError||wpError>maxError
%     fitness=1e-4;
% else
fitness=0;
%     fitness=sens1+sens2;
    fitness=sum([gError wpError qpError])*(1.75+max([sens1 sens2]));
    fitness=1/fitness;
% end
end