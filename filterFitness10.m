function fitness=filterFitness10(valuesArray,objectivesArray)
[gError,wpError,qpError,~,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
if sum([gError wpError qpError]>3.5)>0||sum([sens1 sens2]>.35)>0
if gError<3.5
    gError=3.5;
end
if qpError<3.5
    qpError=3.5;
end
if wpError<3.5
    wpError=1;
end
if sens1<.35
    sens1=.35;
end
if sens2<.35
    sens2=.35;
end

end
fitness=sum([gError wpError qpError])+100*(sens1+sens2);
fitness=1/fitness;
end

