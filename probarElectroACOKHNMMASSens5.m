estadisticasContainer=containers.Map;
tic
win=randperm(100);
% win=win(1);
win=1001;


for Q=[3];
for numeroDeHormigas=[18 ];
numeroDeIteraciones=1300 %9500%13000;
for mutationRate=[0]% 1.5 ];
for initialPheromone=[ .0025 ];
for pheromoneFactor=[inf ]%2.1 ];
for rho=[.83];
metodoDeSeleccion='uniform';
escenario=2;
for alpha=[ .83]
for funcionDeCosto=[15] 
for ejecucion=1:1
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR12T(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,Q,alpha,0,5,metodoDeSeleccion,escenario,funcionDeCosto,win,pheromoneFactor,mutationRate)
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor funcionDeCosto mutationRate win escenario]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN2_sensibilidad_R12'  '_93Ts.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc

end
end
end
end
end
end
end
end
end


win
toc
format shortg
c = clock
