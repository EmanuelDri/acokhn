estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=200
    for numeroDeHormigas=350
        for rho=[0.1 .26]
            %             for alpha=1:5
            %                 for beta=1:5
            for pheromoneFactor=[32.5]
                for alpha=[0.9 1 1.25]
                beta=0;
                for Q=3
                    for initialPheromone=[  0.5 0.9 1 2 3 45]
                    for iteration=1:5
                        
                        tic
                        
                        [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurable(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,3,alpha,beta,inf,'ordered roulette',2,3,7,pheromoneFactor);
                        linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones initialPheromone pheromoneFactor]);
                        linea=[linea '\n'];
                        archivo=fopen(['KHN_informe_MMAS_parametros_buenos_algoritmo_base1_reducido'  '.txt'],'a');
                        fprintf(archivo,linea);
                        fclose(archivo);
                        
                        toc
                    end
                    end
                end
                                end
            end
        end
    end
end

toc
format shortg
c = clock
