function fitness=euclideanFilterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,maxError,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
if gError>maxError||qpError>maxError||wpError>maxError
    fitness=0;
else
    fitness=norm([sens1 sens2]);
    fitness=fitness+norm([gError wpError qpError]);
    fitness=1/fitness;
end
end