function fitness=edoSenseiNoFilterFitness(valuesArray,objectivesArray)
[gError,wpError,qpError,maxError,sens1,sens2]=fitnessBaseOperations(valuesArray,objectivesArray);
    fitness=ones(3,1)'*([gError wpError 1.1*qpError])';
    fitness=1/fitness;
end