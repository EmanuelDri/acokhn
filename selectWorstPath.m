function [route,cost,ant]=selectWorstPath(antsSolutions,fitnessFunction,objectivesArray,antsNumber)
cost=inf;
ant=0;
for antInd=1:antsNumber
    antSolution=antsSolutions(antInd,:);
    adequacy=feval(fitnessFunction,antSolution,objectivesArray);
%     adequacy=1/adequacy;       
    if adequacy<=cost
        cost=adequacy;
        route=antSolution;
		ant=antInd;
    end
    
end
cost=1/cost;
end