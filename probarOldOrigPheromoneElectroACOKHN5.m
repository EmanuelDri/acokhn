estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[200]
    for numeroDeHormigas=[350]
        for rho=[.1 .26]
            for pheromoneInitial= [  0.5 0.9 2 3 45]
%                 for pheromoneFactor=[32.5 27.5]
                    %             for alpha=1:5
                    %                 for beta=1:5
                    for alpha=[0.9 1 1.25]
                        for Q=3
                            for ejecucion=1:5;
                                
                                tic
                                
                                [route,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACO(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),pheromoneInitial,Q,alpha,0,inf,'ordered roulette',2,3,4)
                                linea=num2str([route,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeHormigas numeroDeIteraciones pheromoneInitial]);
                                linea=[linea '\n'];
                                archivo=fopen(['KHN_informe_probar_original_buena_config_reducido_apurado'  '.txt'],'a');
                                fprintf(archivo,linea);
                                fclose(archivo);
                                
                                toc
                                
                            end
                        end
                    end
                end
            end
        end
    end
% end

toc
format shortg
c = clock
