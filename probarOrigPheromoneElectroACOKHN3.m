estadisticasContainer=containers.Map;
tic

for numeroDeIteraciones=[450]
    for numeroDeHormigas=[450]
        for rho=.26:.02:.36
            %             for alpha=1:5
            %                 for beta=1:5
            for pheromoneFactor=[27.5 ]
                for alpha=1.25
                    for Q=3
                        for initialPheromone=[.1 3]
                        for ejecucion=1:15;
                            
                            tic
                            
                            [path,cost,gError,wpError,qError,sens1,sens2]=originalPheromoneKHNACOMMASConfigurableR(8,rho,numeroDeHormigas,numeroDeIteraciones,3,2000*pi,1/sqrt(2),initialPheromone,3,alpha,0,inf,'inverse ordered roulette',2,3,9,pheromoneFactor);
                            linea=num2str([path,cost,gError,wpError,qError,sens1,sens2 Q rho alpha numeroDeIteraciones numeroDeHormigas pheromoneFactor initialPheromonez]);
                            linea=[linea '\n'];
                            archivo=fopen(['KHN_fine_tests_rhoR'  '.txt'],'a');
                            fprintf(archivo,linea);
                            fclose(archivo);
                            
                            toc
                        end
                        end
                    end
                end
            end
        end
    end
end

toc
format shortg
c = clock
